-- Lua script of map houses/elder_house.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

function map:on_started()
  --Open cellar door if Elder has given quest and map:
  if game:get_value("has_world_map") then
    map:set_doors_open("cellar_door")
  end
  --Remove blocking fire in basement
  if game:get_value("elder_house_fire_put_out") then
    for e in map:get_entities("fire_") do
      e:remove()
      tricky_fairy:remove()
    end
  end
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end



function elder:on_interaction()
  --determine dialog:
  local dialog_counter
  if not game:has_item("world_map") then
    dialog_counter = 1
  end

  if dialog_counter == 1 then
    --First Interaction:
    game:start_dialog("elder_house.elder.1", game:get_player_name(), function()
      game:start_dialog("elder_house.elder.2_start_quest_question", game:get_player_name(), function()
        hero:start_treasure("world_map", 1, "has_world_map", function()
          game:start_dialog("elder_house.elder.5_cellar", function()
            map:open_doors("cellar_door")
            sol.audio.play_sound("door_open")
          end)
        end)
      end)
    end)
  else
    game:start_dialog("elder_house.elder.default")
  end
end


for fire in map:get_entities("fire_") do
  function fire:on_interaction()
    game:start_dialog("elder_house.blocking_fire")
    game:set_value("elder_house_observed_flames", true)
  end

  function fire:on_interaction_item(item)
    if item:get_name():find("^bottle") and item:get_variant() == 2 then

      -- using water on the fire
      hero:freeze()
      sol.audio.play_sound("splash")
      for e in map:get_entities("fire_") do
        e:remove()
      end
      sol.timer.start(800, function()
        --sol.audio.play_sound("secret")
        game:set_value("elder_house_fire_put_out", true)
        game:start_dialog("elder_house.fairy_praise", function()
          local m = sol.movement.create"straight"
          m:set_angle(math.pi / 2)
          m:set_speed(100)
          m:set_ignore_obstacles(true)
          m:set_max_distance(96)
          m:start(tricky_fairy, function()
            tricky_fairy:remove()
            hero:unfreeze()
          end)
        end)
      end)
      item:set_variant(1) -- make the bottle empty
      return true
    end

    return false
  end
end


function fairy_taunt_sensor:on_activated()
  self:remove()
  if not game:get_value("elder_house_fire_put_out") then
    game:start_dialog("elder_house.fairy_taunt")
  end
end

