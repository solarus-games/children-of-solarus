-- Lua script of enemy hermit crab.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local shell_color = "yellow" -- Default shell color.
local shell -- Shell (destructible entity) of the hermit crab.
local walking_speed, running_speed = 10, 20
local hero_detection_distance = 80
local state = "outside" -- Possible values: "outside", "inside".

function enemy:on_created()
  shell_color = enemy:get_property("shell_color") or shell_color
  local sprite_id = "enemies/" .. enemy:get_breed()
  enemy:create_sprite(sprite_id, "crab")
  enemy:set_life(4)
  enemy:set_damage(2)
  enemy:set_pushed_back_when_hurt(false)
  enemy:create_shell()
  if enemy.set_default_behavior_on_hero_shield then
    enemy:set_default_behavior_on_hero_shield("normal_shield_push")
  end
end

-- Event called when the enemy should start or restart its movements.
function enemy:on_restarted()
  enemy:stop_movement()
  enemy:get_sprite("crab"):set_animation("stopped")
  pause_duration = 250 + math.random() * 2000
  sol.timer.start(enemy, pause_duration, function()
    enemy:start_walking()
  end)
end

-- Move shell with the crab.
function enemy:on_position_changed(x,y,layer)
  if shell ~= nil then enemy:update_shell_position() end
end

-- Create a shell for the hermit crab.
function enemy:create_shell()
  -- Main properties of the shell.
  local breed = "generic_enemy" -- Empty enemy breed.
  local x,y,layer = enemy:get_position()
  local crab_sprite = enemy:get_sprite("crab")
  local dir = crab_sprite:get_direction()
  local prop = {x = x, y = y, layer = layer, direction = dir, breed = breed}
  shell = map:create_enemy(prop)
  shell:set_invincible()
  shell:set_attack_consequence("sword", "protected")
  shell:set_attack_consequence("boomerang", "protected")
  shell:set_attack_consequence("arrow", "protected")
  shell:set_damage(0)
  shell:set_push_hero_on_sword(true)
  shell:set_pushed_back_when_hurt(false)
  -- Shell can be lifted.
  shell:set_weight(0)
  function shell:on_lifting(carrier, carried_object)
    local old_sprite_carried = carried_object:get_sprite()
    local dir = old_sprite_carried:get_direction()
    carried_object:remove_sprite(old_sprite_carried)
    local new_sprite = carried_object:create_sprite("destructibles/shell_yellow")
    new_sprite:set_animation("stopped")
    new_sprite:set_direction(dir)
    function carried_object:on_lifted()
      enemy:on_shell_lifted(carrier, carried_object)
    end
  end
  -- Create and initialize sprite.
  local sprite_id = "enemies/crab_hermit_shell_" .. shell_color
  local shell_sprite = shell:create_sprite(sprite_id)
  enemy:start_updating_shell_sprite()
  shell_sprite:set_animation("stopped")
  local dir = crab_sprite:get_direction()
  shell_sprite:set_direction(dir)
  enemy:update_shell_position()
end

-- Initialization to update shell sprite.
function enemy:start_updating_shell_sprite()
  local sprite = enemy:get_sprite("crab")
  local shell_sprite = shell:get_sprite()
  function sprite:on_animation_changed(enemy_anim)
    local anim = shell_sprite:has_animation(enemy_anim) and enemy_anim or "stopped"
    shell_sprite:set_animation(anim)
  end
  function sprite:on_frame_changed(enemy_anim, frame)
    if (shell_sprite:get_num_frames() > frame) then
      shell_sprite:set_frame(frame)
    end
  end
  function shell_sprite:on_animation_changed(shell_anim)
    -- Allow hidden animation.
    if state == "inside" then
      if shell_anim ~= "hidden" then 
        shell_sprite:set_animation("hidden")
      end
      return
    end
    -- Force enemy animations.
    local enemy_anim = sprite:get_animation()
    local anim = shell_sprite:has_animation(enemy_anim) and enemy_anim or "stopped"
    if (shell_anim ~= anim) then
      shell_sprite:set_animation(anim)
    end
  end
  function sprite:on_direction_changed(anim, dir)
    shell_sprite:set_direction(dir)
    enemy:update_shell_position()
  end
end

-- Update shell position behind the crab.
function enemy:update_shell_position()
  -- Set position of the shell.
  local dir = enemy:get_sprite("crab"):get_direction()
  local x,y,layer = enemy:get_position()
  local pos = {{x=-8,y=0}, {x=0,y=8}, {x=8,y=0}, {x=0,y=-8}}
  x,y = x + pos[dir + 1].x, y + pos[dir + 1].y
  shell:set_position(x,y,layer)
end

-- Restart if a movement has finished.
function enemy:on_movement_finished() enemy:restart() end

-- Move randomly.
function enemy:start_walking()
  -- Select movement speed, depending on hero distance.
  local distance_to_hero = enemy:get_distance(hero)
  local speed = walking_speed
  if (distance_to_hero  < hero_detection_distance) then
    speed = running_speed
  end
  -- Select random directions to "sidewalk". 
  local facing_direction = math.random(0, 3)
  local movement_direction = (facing_direction + (2 * math.random(0,1) - 1)) % 4
  local crab_sprite = enemy:get_sprite("crab")
  crab_sprite:set_direction(facing_direction)
  crab_sprite:set_animation("walking")
  -- Create movement.
  local m = sol.movement.create("straight")
  m:set_smooth(true)
  m:set_angle(movement_direction * math.pi / 2)
  m:set_speed(speed)
  m:set_max_distance(math.random(32, 80))
  local after_movement = function()
    local needs_hide = math.random(0, 1) == 1
    if needs_hide then enemy:hide_in_shell()
    else enemy:restart() end
  end
  function m:on_finished() after_movement() end
  function m:on_obstacle_reached() after_movement() end
  m:start(enemy) -- Start movement.
 end

-- Hide enemy for a while in the shell.
function enemy:hide_in_shell()
  state = "inside"
  local duration_hide = 3000 + math.random() * 2000
  local shell_sprite = shell:get_sprite()
  shell_sprite:set_animation("hidden")
  sol.timer.start(shell, duration_hide, function()
    enemy:set_enabled(true) -- Make enemy reappear.
    state = "outside"
    shell_sprite:set_animation("stopped")
  end)
  enemy:set_enabled(false)
end

-- Behavior of the crab when the shell is lifted.
function enemy:on_shell_lifted(carrier, carried_object)
  -- If the crab is outside, replace with normal crab enemy.
  if state == "outside" and enemy:exists() then
    local x,y,layer = enemy:get_position()
    local dir = enemy:get_sprite("crab"):get_direction()
    local prop = {x=x, y=y, layer=layer, direction=dir, breed="crab"}
    map:create_enemy(prop)
  end
  -- Break shell when thrown.
  function carried_object:on_breaking()
    -- If crab is inside, create new crab when the carried object breaks.
    local x,y,layer = carried_object:get_position()
    local dir = math.random(0, 3)
    if state == "inside" then
      local prop_enemy = {x=x, y=y, layer=layer, direction=dir, breed="crab"}
      map:create_enemy(prop_enemy)
    end
    -- Start breaking animation.
    local sprite_set = "destructibles/shell_" .. shell_color
    local prop_custom = {x=x, y=y, layer=layer, direction=dir, width=16, height=16, sprite=sprite_set}
    local custom_breaking = map:create_custom_entity(prop_custom)
    local sprite_breaking = custom_breaking:get_sprite()
    sprite_breaking:set_animation("destroy")
    function sprite_breaking:on_animation_finished() custom_breaking:remove() end
    carried_object:remove()
  end
  -- Destroy old crab.
  enemy:remove()
end

-- Replace shell sprite.
function enemy:on_dying()
  local shell_sprite = shell:get_sprite()
  local dir = shell_sprite:get_direction()
  shell:remove_sprite(shell_sprite)
  shell_sprite = shell:create_sprite("destructibles/shell_" .. shell_color)
  shell_sprite:set_animation("stopped")
  shell_sprite:set_direction(dir)
end
