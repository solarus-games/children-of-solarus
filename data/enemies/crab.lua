-- Lua script of enemy crab.
local enemy = ...
local game = enemy:get_game()
local map = enemy:get_map()
local hero = map:get_hero()
local walking_speed, running_speed = 20, 100
local hero_detection_distance = 80

-- Event called when the enemy is initialized.
function enemy:on_created()
  enemy:create_sprite("enemies/" .. enemy:get_breed())
  enemy:set_life(2)
  enemy:set_damage(1)
end

-- Event called when the enemy should start or restart its movements.
function enemy:on_restarted()
  enemy:stop_movement()
  enemy:get_sprite():set_animation("stopped")
  local pause_duration = 250 + math.random() * 2000
  sol.timer.start(enemy, pause_duration, function()
    enemy:start_walking()
  end)
end
-- Restart if a movement has finished.
function enemy:on_movement_finished() enemy:restart() end

-- Move randomly.
function enemy:start_walking()
  -- Select movement speed, depending on hero distance.
  local distance_to_hero = enemy:get_distance(hero)
  local speed = walking_speed
  if (distance_to_hero  < hero_detection_distance) then
    speed = running_speed
  end
  -- Select random directions to "sidewalk". 
  local facing_direction = math.random(0, 3)
  local movement_direction = (facing_direction + (2 * math.random(0,1) - 1)) % 4
  enemy:get_sprite():set_direction(facing_direction)
  enemy:get_sprite():set_animation("walking")
  -- Create movement.
  local m = sol.movement.create("straight")
  m:set_smooth(true)
  m:set_angle(movement_direction * math.pi / 2)
  m:set_speed(speed)
  m:set_max_distance(math.random(32, 80))
  function m:on_finished() enemy:restart() end
  function m:on_obstacle_reached() enemy:restart() end
  m:start(enemy) -- Start movement.
 end
